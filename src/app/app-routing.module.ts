import {Router, RouterModule, Routes} from '@angular/router';
import {FolderListComponent} from './components/folder/folder-list/folder-list.component';
import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FolderContentComponent} from './components/folder/folder-content/folder-content.component';

const routes: Routes = [
  {
    path: 'list',
    component: FolderListComponent
  },
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full'
  },
  {
    path: 'content',
    component: FolderContentComponent
  }
];

@NgModule({
imports: [RouterModule.forRoot(routes), NgbModule],
exports: [RouterModule]
})
export class AppRoutingModule { }
export class YourAppModule { }
