import { Component, OnInit } from '@angular/core';
import {FolderService} from '../../../services/folder-service';
import {FolderDto} from '../../../models/folder-dto';
import {FormControl, FormGroup} from '@angular/forms';
import {FolderCommand} from '../../../models/folder-command';
import {Router} from '@angular/router';

@Component({
  selector: 'app-folder-content',
  templateUrl: './folder-content.component.html',
  styleUrls: ['./folder-content.component.scss']
})
export class FolderContentComponent implements OnInit {

  folders: FolderDto [] = [];
  basicForm: FormGroup;
  folderCommand: FolderCommand = new FolderCommand();

  constructor(private folderService: FolderService, private router: Router) {
    const navigation = this.router.getCurrentNavigation();
    if (navigation.extras.state != null){
      this.folderCommand = navigation.extras.state as FolderCommand;
    }
    this.basicForm = new FormGroup({
      id: new FormControl(this.folderCommand.id),
    });
  }
  ngOnInit() {
  }

  content(folder: FolderDto) {
  this.folderService.content(folder).then(response => {
    this.folders;
  });
  }
}
