import {Component, OnInit} from '@angular/core';
import {FolderService} from '../../../services/folder-service';
import {FolderDto} from '../../../models/folder-dto';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-folder-list',
  templateUrl: './folder-list.component.html',
  styleUrls: ['./folder-list.component.scss']
})
export class FolderListComponent implements OnInit {

  folders: FolderDto [] = [];

  constructor(private folderService: FolderService) { }

  ngOnInit() {
  }

  fetch() {
    this.folderService.fetch().then(response => {
         this.folders = response;
  });
  }

}
