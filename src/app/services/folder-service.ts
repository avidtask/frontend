import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {FolderDto} from '../models/folder-dto';
import {Observable} from 'rxjs';
import {Folder} from '../models/folder';

@Injectable({
  providedIn: 'root'
})
export class FolderService {

  constructor(private httpClient: HttpClient) { }

fetch() {
    return this.httpClient.get('http://localhost:8080/GET/' ).toPromise().then(response => response as FolderDto[]);
}

content(folderDto: FolderDto) {
    return this.httpClient.get('http://localhost:8080/GET/id/' + folderDto.id).toPromise();
}

}
